package com.sis.sunshinemobileapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class registration extends AppCompatActivity {
    private static final String TAG = "registration";
    private FirebaseAuth mAuth;
    EditText email,password;
    Button btn;
    EditText text1, text2;
    private ProgressDialog progress;
    public static String UserDocId;

    public static final String NAME_KEY = "NAME";
    public static final String PHONE_KEY = "PHONE NUMBER";
    public static final String BVN_KEY = "BVN";
    public static final String EMAIL_KEY = "EMAIL";
    public static final String PASSWORD_KEY = "PASSWORD";


    FirebaseFirestore db = FirebaseFirestore.getInstance();



    public void registrati(){


        progress=new ProgressDialog(this);
        progress.setMessage("Signing Up User");
        //  progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.setProgress(0);
        progress.show();

        mAuth = FirebaseAuth.getInstance();

        EditText textreg = findViewById(R.id.email);
        final String email = textreg.getEditableText().toString();

        EditText textpasswordreg = findViewById(R.id.password);
        final String password = textpasswordreg.getEditableText().toString();

        EditText Suintext = findViewById(R.id.Suin);
        final String SuinReg = Suintext.getEditableText().toString();


        if(SuinReg.equals("stu6238") ||  SuinReg.equals("stu6382") ||SuinReg.equals("stu6832") ||SuinReg.equals("stu6283") ||SuinReg.equals("stu6328") ||SuinReg.equals("stu6823") ||SuinReg.equals("stu9875") ||SuinReg.equals("stu9758") ||SuinReg.equals("stu9578") ||SuinReg.equals("stu9857") ||SuinReg.equals("stu9785") ||SuinReg.equals("stu9587")){
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(registration.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();

                                Intent i = new Intent();
                                i.setClass(registration.this, login.class);
                                startActivity(i);
                                finish();

                            } else {

                                progress.cancel();
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                Toast.makeText(registration.this, "Registration failed" +
                                                "Invalid Fields",
                                        Toast.LENGTH_SHORT).show();



                            }

                            // ...
                        }
                    });
        }else if(SuinReg.equals("sta2345") ||  SuinReg.equals("sta2453") ||SuinReg.equals("sta2543") ||SuinReg.equals("sta2354") ||SuinReg.equals("sta2435") ||SuinReg.equals("sta2534") ){
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(registration.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();

                                Intent i = new Intent();
                                i.setClass(registration.this, login.class);
                                startActivity(i);
                                finish();

                            } else {

                                progress.cancel();
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                Toast.makeText(registration.this, "Registration failed" +
                                                "Invalid Fields",
                                        Toast.LENGTH_SHORT).show();



                            }

                            // ...
                        }
                    });
        } else {

            Toast.makeText(registration.this, "Incorrect SUIN ask the school administration for one  ",
                    Toast.LENGTH_SHORT).show();
        }




    }




    public void backletreg() {


        Intent r = new Intent();
        r.setClass(registration.this, login.class);
        startActivity(r);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        MultiDex.install(this);

        TextView backreg = findViewById(R.id.textView4);
// Register the onClick listener with the implementation above
        backreg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                backletreg();
                //DO SOMETHING! {RUN SOME FUNCTION ... DO CHECKS... ETC}
            }


        });

        Button reg = findViewById(R.id.button2);
// Register the onClick listener with the implementation above
        reg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {

                // registrati();
                saveregdetails();
                //DO SOMETHING! {RUN SOME FUNCTION ... DO CHECKS... ETC}
            }
        });


    }



    public void saveregdetails(){


        EditText name = findViewById(R.id.name);
        EditText phonenumber = findViewById(R.id.phonenumber);
        //   EditText bvn = findViewById(R.id.bvn);
        EditText email = findViewById(R.id.email);
        EditText password = findViewById(R.id.password);

        String nametext = name.getText().toString();
        String phonenumbertext = phonenumber.getText().toString();
        // String bvntext = bvn.getText().toString();
        String emailtext = email.getText().toString();
        String passwordtext = password.getText().toString();


        if (nametext.isEmpty() || phonenumbertext.isEmpty() ||  emailtext.isEmpty() || passwordtext.isEmpty() ){




            // Toast.makeText(registration.this, "",
            //                Toast.LENGTH_LONG).show();


            Toast.makeText(registration.this, "Your password must be more than 6 character longer",
                    Toast.LENGTH_SHORT).show();

            return;

        }else {



            registrati();


            UserDocId = emailtext + "userdocfile";

            Map<String, Object> user = new HashMap<>();
            user.put(NAME_KEY, nametext);
            user.put(PHONE_KEY, phonenumbertext);
            // user.put(BVN_KEY, bvntext);
            user.put(EMAIL_KEY, emailtext);
            user.put(PASSWORD_KEY, passwordtext);
//        mDocRef.put(dataToSave);


            db.collection("user").document(UserDocId)
                    .set(user)

                    .addOnSuccessListener(new OnSuccessListener() {
                        @Override
                        public void onSuccess(Object o) {
                        }

                    }).addOnFailureListener(new OnFailureListener(){
                @Override
                public void onFailure(@NonNull Exception e){

                    Log.w(TAG, "Error adding document", e);
                }
            });



        }




//                .addonSuccessListener(new OnSuccessListener<Void>(){
//            public void onSuccess(Void aVoid){
//                Log.d(TAG, "details successfuly added to th database");
//            }
//        }).addOnFailureListener(new OnFailureListener(){
//
//            public void onFailure(Exception e){
//                Log.w(TAG, "DOCUMENT WAS NOT SAVED");
//            }
//        });

    }

//
//
//        GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();
//        FirebaseOptions options = new FirebaseOptions.Builder()
//                .setCredentials(credentials)
//                .setProjectId(projectId)
//                .build();
//        FirebaseApp.initializeApp(options);
//
//        Firestore db = FirestoreClient.getFirestore();
//
//
//
//        DocumentReference docRef = db.collection("user").document("alovelace");
//// Add document data  with id "alovelace" using a hashmap
//        Map<Strin g, Object> data = new HashMap<>();
//        data.put(username, "Ada");
//        data.put("last", "Lovelace");
//        data.put("born", 1815);
////asynchronously write data
//        ApiFuture<WriteResult> result = docRef.set(data);
// ...
// result.get() blocks on response








}


