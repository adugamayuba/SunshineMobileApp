package com.sis.sunshinemobileapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.Transaction;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;

public class FeesPayment extends AppCompatActivity {




    private static final String TAG = "FeesPayment";
    private FirebaseAuth mAuth;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    //   DatabaseReference d = com.google.firebase.database.DatabaseReference.goOnline();
    private DatabaseReference mDatabase;

    public static final String loan_email = "Request Email Address";
    public static final String loan_request_name = "Request Name";
    public static final String NAME_KEY = "NAME";
    public static final String card_number = "Card Number";
    public static final String card_expiryMonth = "Card Expiry Month";
    public static final String card_expiryYear = "Card Expiry Year";
    public static final String card_cvv = "Card Cvv";
    public static final String Ema_il = "Email";

    public static TextView textDisplay;

    private Card card;
    private Charge charge;


    String cardNumber;
    //  int expiryMonth;
    //  int expiryYear;


    String emonthint;
    String expiryYearint;
    String cvv;
    String email;
    String fullname;

    EditText expiry;
    EditText cnumber;
    EditText emonth;
    EditText usercvv;
    EditText useremail;
    EditText name;
    int expiryMonth;
    int expiryYear;
    String uemailinput;
    String m_Text;

    //DatabaseReference d;
//    DatabaseReference mDatabase;
    String key;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fees_payment);



//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        textDisplay = findViewById(R.id.textView12);









        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Email Address");

        // Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT );
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String   mm_Text = input.getText().toString();
                final String m_Text = mm_Text;
                if(mm_Text.isEmpty()){


                }
//                if(mm_Text.isEmpty()){
//
//                }

                mDatabase = FirebaseDatabase.getInstance().getReference();
                //  String key = mDatabase.child("user").push().getKey();

                String uemailinput = m_Text + "userdocfile";

                DocumentReference user = db.collection("user").document(uemailinput);
                user.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot doc = task.getResult();
                            StringBuilder fields = new StringBuilder("");
                            fields.append("").append(doc.get("Interst"));

                            textDisplay.setText(fields.toString());


                        }
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                            }
                        });

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent ex = new Intent();

                ex.setClass(FeesPayment.this, MainActivity.class);
                startActivity(ex);
                finish();

            }
        });

        builder.show();


















        PaystackSdk.initialize(getApplicationContext());


        //ReadSingleContact();
        //  validateForm();
        Button gloan = findViewById(R.id.button8);
// Register the onClick listener with the implementation above

        gloan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                combinations();
                //DO SOMETHING! {RUN SOME FUNCTION ... DO CHECKS... ETC}
            }
        });


        //  if(fullname.isEmpty()||email.isEmpty()||card_number.isEmpty()){
        //   Log.w(TAG, "Please fill in the empty fields");
        //  }else{


//
//        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String key = getRef(position).getKey();
//
//            }
//        });
//


//        mDatabase
//        DatabaseReference firebaseRef = d;
//        String uid = firebaseRef.child("users").push().getKey();
//        Log.i("uid", uid);


    }


    public void thankyou() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Thank you");
        alertDialogBuilder.setMessage("Thank you for paying back the loan collected we do really appreciate");

        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }


    public void combinations() {
        // validateForm();
        //  checking();
        validatingcard();
        // performCharge();
        //  senddetails();

    }

//
    // public void checking(){


    // }

    public void validatingcard() {


        EditText cnumber = findViewById(R.id.card_number);
        final String cardNumber = cnumber.getText().toString();


        EditText expiry = findViewById(R.id.expiryyear);
        String expiryYearint = expiry.getText().toString();

        try{

            expiryYear = Integer.parseInt(expiryYearint);
            // int cypher = Integer.parseInt(cypherInput.getText().toString());
            // final int fexpiryYear = expiryYear;
        }catch(NumberFormatException ex){ // handle your exception

        }



        EditText emonth = findViewById(R.id.expirymonth);
        String emonthint = emonth.getText().toString();


        try{
            expiryMonth = Integer.parseInt(emonthint);
            // final int fexpiryMonth = expiryMonth;

        }catch(NumberFormatException ex){ // handle your exception

        }



        EditText usercvv = findViewById(R.id.cvv);
        final String cvv = usercvv.getText().toString();

        EditText useremail = findViewById(R.id.useremail);
        final String email = useremail.getText().toString();


        EditText name = findViewById(R.id.editText18);
        final String fullname = name.getText().toString();


        if (cardNumber.isEmpty() || expiryYearint.isEmpty() || emonthint.isEmpty() || cvv.isEmpty() || email.isEmpty() || fullname.isEmpty()) {
            boolean valid = true;


            if (TextUtils.isEmpty(cardNumber)) {
                cnumber.setError("Required.");
                valid = false;
            } else {
                cnumber.setError(null);
            }


            if (TextUtils.isEmpty(emonthint)) {
                emonth.setError("Required.");
                valid = false;
            } else {
                emonth.setError(null);
            }


            if (TextUtils.isEmpty(expiryYearint)) {
                expiry.setError("Required.");
                valid = false;
            } else {
                expiry.setError(null);
            }


            if (TextUtils.isEmpty(cvv)) {
                usercvv.setError("Required.");
                valid = false;
            } else {
                usercvv.setError(null);
            }


            if (TextUtils.isEmpty(email)) {
                useremail.setError("Required.");
                valid = false;
            } else {
                useremail.setError(null);

            }
            return;


        } else {


//        if (emonthint.isEmpty()){
//
//            Toast.makeText(pay_loan.this, "Please fill in all the field continue",
//                    Toast.LENGTH_SHORT).show();
//
//        }
//checking();

//        if(cardNumber.isEmpty() ||  cvv.isEmpty() || email.isEmpty() || fullname.isEmpty()){
//
//
//            Toast.makeText(pay_loan.this, "Card not Valid", Toast.LENGTH_LONG).show();
//
//
//            } else {
            //do something

            // if(docid == null){
            //   Toast.makeText(pay_loan.this, "You have no outstsnding payments", Toast.LENGTH_SHORT).show();


            //   }else {


            // }



            Card card = new Card(cardNumber, expiryMonth, expiryYear, cvv);
            if (card.isValid()) {
                // charge card
                Toast.makeText(FeesPayment.this, "Card is Valid", Toast.LENGTH_LONG).show();
                // performCharge();


                charge = new Charge();

                //set the card to charge
                charge.setCard(card);

                //call this method if you set a plan
                //charge.setPlan("PLN_yourplan");

                charge.setEmail(email); //dummy email address
                String re = textDisplay.getText().toString();


                if(re.equals("N/A")){

                    Toast.makeText(FeesPayment.this, "Transaction failed you don't have any outsanding loan yet please contact us for any proplem ", Toast.LENGTH_LONG).show();


                }else {

                    int reamount = Integer.parseInt(re);
                    int realamount = reamount * 100;


                    charge.setAmount(realamount); //test amount

                    PaystackSdk.chargeCard(FeesPayment.this, charge, new Paystack.TransactionCallback() {
                        @Override
                        public void onSuccess(Transaction transaction) {
                            // This is called only after transaction is deemed successful.
                            // Retrieve the transaction, and send its reference to your server
                            // for verification.
                            String paymentReference = transaction.getReference();
                            Toast.makeText(FeesPayment.this, "Transaction Successful! payment reference: "
                                    + paymentReference, Toast.LENGTH_LONG).show();
                            thankyou();

                            Map<String, Object> Paid_Loan = new HashMap<>();
                            Paid_Loan.put("FULLNAME", fullname);
                            Paid_Loan.put("EMAIL ADDRESS", email);
                            Paid_Loan.put("CARD NUMBER", cardNumber);
                            Paid_Loan.put("CARD EXPIRY MONTH", expiryMonth);
                            Paid_Loan.put("CARD EXPIRY YEAR", expiryYear);
                            Paid_Loan.put("CARD CVV", cvv);


                            db.collection("Paid_Loan")
                                    .add(Paid_Loan)
                                    .addOnSuccessListener(new OnSuccessListener() {
                                        @Override
                                        public void onSuccess(Object o) {

                                        }


                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {

                                    Log.w(TAG, "Error adding document", e);
                                }
                            });
                        }

                        @Override
                        public void beforeValidate(Transaction transaction) {
                            // This is called only before requesting OTP.
                            // Save reference so you may send to server. If
                            // error occurs with OTP, you should still verify on server.
                        }

                        @Override
                        public void onError(Throwable error, Transaction transaction) {
                            //handle error here
                        }
                    });
                }

            } else {

                Toast.makeText(FeesPayment.this, "Payment failed make sure all details provided are correct", Toast.LENGTH_LONG).show();
            }


        }


    }


    //  private void performCharge() {
    //create a Charge object

    // }


    //     private boolean validateForm(){


    //    }


//this.do = Get_loan.docid;


    // private void ReadSingleContact() {





//public void senddetails(){


}





























